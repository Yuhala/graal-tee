/* Generated by GraalVM SGXProxyGenerator. */ 

#if defined(__cplusplus)
extern "C" {
#endif

void ocall_relay_Main(graal_isolatethread_t* param_0, int param_1)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_Main(NULL, param_1);

}

void ocall_relay_addObjs(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, int param_4, int param_5)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_addObjs(NULL, param_1, param_2, param_3, param_4, param_5);

}

void ocall_relay_doConcreteIn(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, char* param_4, int param_5)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_doConcreteIn(NULL, param_1, param_2, param_3, param_4, param_5);

}

void ocall_relay_doConcreteOut(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, char* param_4, int param_5)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_doConcreteOut(NULL, param_1, param_2, param_3, param_4, param_5);

}

void ocall_relay_doConsistencyTest(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, int param_4, int param_5)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_doConsistencyTest(NULL, param_1, param_2, param_3, param_4, param_5);

}

void ocall_relay_doProxyOut(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, int param_4)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_doProxyOut(NULL, param_1, param_2, param_3, param_4);

}

void ocall_relay_gcTest(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, int param_4, int param_5)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_gcTest(NULL, param_1, param_2, param_3, param_4, param_5);

}

int ocall_relay_getRandString(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, int param_4)
{

GRAAL_SGX_INFO();
int ret;
graalsgx_ocall_relay_getRandString(&ret,NULL, param_1, param_2, param_3, param_4);

return ret;
}

void ocall_relay_doProxyIn(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, int param_4)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_doProxyIn(NULL, param_1, param_2, param_3, param_4);

}

void ocall_relay_removeObjs(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, int param_4, int param_5)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_removeObjs(NULL, param_1, param_2, param_3, param_4, param_5);

}

int ocall_relay_getName(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3)
{

GRAAL_SGX_INFO();
int ret;
graalsgx_ocall_relay_getName(&ret,NULL, param_1, param_2, param_3);

return ret;
}

void ocall_relay_Person(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_Person(NULL, param_1, param_2, param_3);

}

int ocall_relay_getPersonId(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3)
{

GRAAL_SGX_INFO();
int ret;
graalsgx_ocall_relay_getPersonId(&ret,NULL, param_1, param_2, param_3);

return ret;
}

void ocall_relay_setId(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, int param_4)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_setId(NULL, param_1, param_2, param_3, param_4);

}

void ecall_doProxyCleanupIn(graal_isolatethread_t* param_0)
{/* Do nothing */}
void ocall_mirrorCleanupOut(graal_isolatethread_t* param_0, int param_1)
{

GRAAL_SGX_INFO();
graalsgx_ocall_mirrorCleanupOut(NULL, param_1);

}

void ecall_mirrorCleanupIn(graal_isolatethread_t* param_0, int param_1)
{/* Do nothing */}
void ocall_relay_Untrusted(graal_isolatethread_t* param_0, int param_1, int param_2)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_Untrusted(NULL, param_1, param_2);

}

void ocall_relay_sayMyName(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, char* param_4, int param_5)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_sayMyName(NULL, param_1, param_2, param_3, param_4, param_5);

}

int ocall_relay_getRandStringU(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, int param_4)
{

GRAAL_SGX_INFO();
int ret;
graalsgx_ocall_relay_getRandStringU(&ret,NULL, param_1, param_2, param_3, param_4);

return ret;
}

void ocall_relay_setNameU(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, char* param_4, int param_5)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_setNameU(NULL, param_1, param_2, param_3, param_4, param_5);

}

void ocall_relay_setNamesU(graal_isolatethread_t* param_0, int param_1, char* param_2, int param_3, int param_4)
{

GRAAL_SGX_INFO();
graalsgx_ocall_relay_setNamesU(NULL, param_1, param_2, param_3, param_4);

}

#if defined(__cplusplus)
}
#endif
